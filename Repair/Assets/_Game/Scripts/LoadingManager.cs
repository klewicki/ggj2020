﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{
    public static LoadingManager Instance;

    public string[] firstLevelScenes;

    public string firstLevelScene;

    public string secondLevelScene;

    public string gameWonScene;

    public GameObject LoadingObscurer;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void LoadFirstLevel()
    {
        StartCoroutine(LoadFirstLevelC());
    }

    public void LoadSecondLevel()
    {
        StartCoroutine(LoadSecondLevelC());
    }
    
    public IEnumerator LoadFirstLevelC()
    {
        LoadingObscurer.SetActive(true);
        
        for(int i = 0; i < firstLevelScenes.Length; i++)
        {
            yield return SceneManager.LoadSceneAsync(firstLevelScenes[i], i == 0 ? LoadSceneMode.Single : LoadSceneMode.Additive);
        }
        
        LoadingObscurer.SetActive(false);
    }

    public IEnumerator LoadSecondLevelC()
    {
        LoadingObscurer.SetActive(true);

        yield return SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(firstLevelScene));
        
        yield return SceneManager.LoadSceneAsync(secondLevelScene, LoadSceneMode.Additive);
        
        LoadingObscurer.SetActive(false);
    }

    public void LoadWinScene()
    {
        SceneManager.LoadScene(gameWonScene);
    }
    
}
