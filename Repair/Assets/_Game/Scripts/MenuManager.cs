﻿using UnityEngine;


public class MenuManager : MonoBehaviour
{
    public void StartGame()
    {
        LoadingManager.Instance.LoadFirstLevel();
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}
