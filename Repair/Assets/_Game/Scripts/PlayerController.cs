﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
   public static PlayerController Instance;
   
   public Transform HoldingPoint;

   public bool IsHoldingSomething;
   
   private void Awake()
   {
      Instance = this;
   }
}
