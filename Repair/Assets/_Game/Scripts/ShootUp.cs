﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootUp : MonoBehaviour
{
   public Rigidbody rigidbody;

   public Vector3 Force;
   
   public void Shoot()
   {
      rigidbody.isKinematic = false;
      rigidbody.AddForce(Force, ForceMode.Impulse);
   }
}
