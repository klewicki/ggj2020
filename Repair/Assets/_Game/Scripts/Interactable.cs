﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class CommandEvent : UnityEvent<string>
{
   
}

public class Interactable : MonoBehaviour
{
   [SerializeField]
   private MeshRenderer _meshRenderer;

   [SerializeField]
   private MeshRenderer _meshRenderer2;
   
   [SerializeField]
   private Material _selectedMaterial;

   
   private Material _originalMaterial2;
   private Material _originalMaterial;

   public bool IsInteractable;
   public bool Holdable;
   
   public CommandEvent OnPlayerInteraction;

   public UnityEvent OnSelect;
   public UnityEvent OnDeselect;

   private bool _selected;
   
   private void OnMouseDown()
   {
      if(!IsInteractable)
         return;
      
      if(Holdable && PlayerController.Instance.IsHoldingSomething)
         return;

      if (_selectedMaterial != null)
      {
         if(_meshRenderer != null)
            _meshRenderer.sharedMaterial = _originalMaterial;
         
         if(_meshRenderer2 != null)
            _meshRenderer2.sharedMaterial = _originalMaterial2;
      }
      
      OnPlayerInteraction.Invoke("Grab");
   }

   public void SetInteractable(bool isInteractable)
   {
      IsInteractable = isInteractable;
      
      if(_selected)
         OnDeselect.Invoke();

      _selected = false;
   }

   private void OnMouseEnter()
   {
      if (!IsInteractable)
         return;

      if (Holdable && PlayerController.Instance.IsHoldingSomething)
         return;

      OnSelect.Invoke();
      _selected = true;
      
      if (_selectedMaterial != null)
      {
         _originalMaterial = _meshRenderer.sharedMaterial;
         _meshRenderer.sharedMaterial = _selectedMaterial;

         if (_meshRenderer2 != null)
         {
            _originalMaterial2 = _meshRenderer2.sharedMaterial;
            _meshRenderer2.sharedMaterial = _selectedMaterial;
         }
      }
   }

   private void OnMouseExit()
   {
      if(!IsInteractable)
         return;
      
      if(Holdable && PlayerController.Instance.IsHoldingSomething)
         return;
      
      OnDeselect.Invoke();
      _selected = false;
      
      if (_selectedMaterial != null)
      {
         _meshRenderer.sharedMaterial = _originalMaterial;
         
         if (_meshRenderer2 != null)
         {
            _meshRenderer2.sharedMaterial = _originalMaterial2;
         }
      }
   }
}
