﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimedOutAction : MonoBehaviour
{
    public UnityEvent OnTimeout;
    
    public void StartActionAfterTime(float time)
    {
        Invoke("StartAction", time);
    }

    public void StartAction()
    {
        OnTimeout.Invoke();
    }
    
}
