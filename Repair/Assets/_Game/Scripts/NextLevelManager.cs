﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelManager : MonoBehaviour
{
   public void GoToNextLevel()
   {
      if(LoadingManager.Instance != null)
         LoadingManager.Instance.LoadSecondLevel();
   }

   public void GoToGameWon()
   {
      if(LoadingManager.Instance != null)
         LoadingManager.Instance.LoadWinScene();
   }
}
