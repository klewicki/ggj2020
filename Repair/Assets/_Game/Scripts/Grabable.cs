﻿using UnityEngine;
using UnityEngine.Events;

public class Grabable : MonoBehaviour
{
    private PlayerController _playerController;

    private float _lerpSpeed = 4f;

    private float _distanceEpsilon = 0.001f;

    [SerializeField]
    private Interactable _interactable;

    public AudioSource _audioSource;
    public AudioClip _clip;
    
    private Vector3 _originalPosition;
    private Quaternion _originalRotation;
    
    private bool _isMoving;
    private bool _isHeld;
    private bool _isGoingToTarget;
    
    private Vector3 _targetPosition;
    private Quaternion _targetRotation;

    
    public UnityEvent OnGrab;
    public UnityEvent OnPutBack;
    public UnityEvent OnReachTarget;
    
    private void Start()
    {
        _playerController = PlayerController.Instance;
        
        _originalPosition = transform.position;
        _originalRotation = transform.rotation;
    }
    
    public void Grab()
    {
        _targetPosition = _playerController.HoldingPoint.position;
        _targetRotation = _playerController.HoldingPoint.localRotation;

        _isMoving = true;
        _isHeld = true;
        
        _interactable.SetInteractable(false);
        
        _audioSource.PlayOneShot(_clip);
        
        OnGrab.Invoke();
        
        _playerController.IsHoldingSomething = true;
    }

    public void PutBack()
    {
        _targetPosition = _originalPosition;
        _targetRotation = _originalRotation;

        _isMoving = true;
        _isHeld = false;
        
        _audioSource.PlayOneShot(_clip);
        
        _interactable.SetInteractable(true);
        
        OnPutBack.Invoke();
        
        _playerController.IsHoldingSomething = false;
    }

    public void MoveToPosition(Transform position)
    {
        _targetPosition = position.position;
        _targetRotation = position.rotation;

        _isMoving = true;
        _isHeld = false;
        _isGoingToTarget = true;
        
        
        _audioSource.PlayOneShot(_clip);
        
        _interactable.SetInteractable(true);
        
        _playerController.IsHoldingSomething = false;
    }

    private void Update()
    {
        if (_isHeld)
        {
            if (Input.GetMouseButtonDown(1))
            {
                PutBack();
            }
            
        }
        
        if (!_isMoving)
            return;

        transform.position = Vector3.Lerp(transform.position, _targetPosition, _lerpSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, _targetRotation, _lerpSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, _targetPosition) < _distanceEpsilon)
        {
            _isMoving = false;

            if (_isGoingToTarget)
            {
                OnReachTarget.Invoke();
                _isGoingToTarget = false;
            }
        }
    }
}
