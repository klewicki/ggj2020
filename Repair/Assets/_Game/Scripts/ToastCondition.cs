﻿using UnityEngine;
using UnityEngine.Events;

public class ToastCondition : MonoBehaviour
{
    public bool _cable1;
    public bool _cable2;
    public bool _toasts;

    public UnityEvent OnConditionMet;

    private bool _invoked;

    public bool Cable1
    {
        get => _cable1;
        set => _cable1 = value;
    }

    public bool Cable2
    {
        get => _cable2;
        set => _cable2 = value;
    }

    public bool Toasts
    {
        get => _toasts;
        set => _toasts = value;
    }

    private void Update()
    {
        if(_invoked)
            return;
        
        if (Cable1 && Cable2 && Toasts)
        {
            OnConditionMet.Invoke();
            _invoked = true;
        }
    }
}
